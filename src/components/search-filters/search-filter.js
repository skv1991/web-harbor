import React, { Component } from 'react';

import './search-filter.scss';

export default class SearchFilter extends Component {

    render() {
        return (
            <div className="search-filter">
                <div className="search-filter--input">
                    <input type="text" />
                    <button>GO</button>
                </div>
            </div>
        )
    }
}
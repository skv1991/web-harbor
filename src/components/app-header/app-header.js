import React, { Component } from 'react';

import './app-header.scss';

import MenuIcon from '../menu-icon';
import LogoImage from './logo.png';

export default class AppHeader extends Component {

    constructor() {
        super();

        this.state = {
            mainTopMenuItems: [
                { id: 1, name: 'Килевые', link: '' },
                { id: 2, name: 'Моторные', link: '' },
                { id: 3, name: 'Гоночные', link: '' },
                { id: 4, name: 'Парусные', link: '' },
                { id: 5, name: 'Истории яхт', link: '' },
                { id: 6, name: 'Журнал', link: '' },
            ],
            rightTopMenuItems: [
                { id: 1, name: 'Форумы', link: '' },
                { id: 2, name: 'Дилерам', link: '' },
                { id: 3, name: 'Продать', link: '' },
                { id: 4, name: 'Войти', link: '' },
            ],
            bottomLeftMenuItems: [
                { id: 1, name: 'Объявления', link: '' },
                { id: 2, name: 'Дилеры', link: '' },
                { id: 3, name: 'Каталог', link: '' },
                { id: 4, name: 'Отзывы', link: '' },
                { id: 5, name: 'Видео', link: '' },
            ],
            bottomRightMenuItems: [
                { id: 1, name: '1', link: '' },
                { id: 2, name: '2', link: '' },
                { id: 3, name: '3', link: '' },
                { id: 4, name: '4', link: '' },
            ],
        }
    }

    render() {

        const mainTopMenuItems = this.state.mainTopMenuItems.map(({ id, name, link }) => {
            return (
                <li key={ id }>
                    <a href={ link }>
                        { name }
                    </a>
                </li>
            )
        });

        const rightTopMenuItems = this.state.rightTopMenuItems.map(({ id, name, link }) => {
            return (
                <li key={ id }>
                    <a href={ link }>
                        { name }
                    </a>
                </li>
            )
        });

        const bottomLeftMenuItems = this.state.bottomLeftMenuItems.map(({ id, name, link }) => {
            return (
                <li key={ id }>
                    <a href={ link }>
                        { name }
                    </a>
                </li>
            )
        });

        const bottomRightMenuItems = this.state.bottomRightMenuItems.map(({ id, name, link }) => {
            return (
                <li key={ id }>
                    <a href={ link }>
                        { name }
                    </a>
                </li>
            )
        });

        return (
            <header className="app-header">
                <div className="app-header--menu-top">
                    <div className="app-header--menu-top-inner">
                        <button className="app-header--burger-menu">
                            <MenuIcon />
                        </button>

                        <a className="app-header--logo"
                           href="/">
                            <img className=""
                                 src={ LogoImage }
                                 alt="" />
                        </a>

                        <ul className="app-header--menu-main">
                            { mainTopMenuItems }
                        </ul>

                        <ul className="app-header--menu-main-right">
                            { rightTopMenuItems }
                        </ul>
                    </div>
                </div>
                <div className="app-header--menu-bottom">
                    <div className="app-header--menu-bottom-inner">
                        <ul className="app-header--menu-sub-left">
                            { bottomLeftMenuItems }
                        </ul>
                        <ul className="app-header--menu-sub-right">
                            { bottomRightMenuItems }
                        </ul>
                        <ul></ul>
                    </div>
                </div>
            </header>
        )
    }
}
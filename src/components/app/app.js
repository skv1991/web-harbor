import React, { Component } from 'react';

import './app.scss';
import AppHeader from "../app-header";
import SearchFilter from "../search-filters";

export default class App extends Component {

    constructor() {
        super();

        this.state = {
            offerItems: [
                {id: 1, name: 'Polar Star Explorer', price: 2600000, image: 'https://cdn.boatinternational.com/bi_prd/bi/library_images/DrN3WevZS2KMltyw3LFc_polar-star-explorer-yacht-1280x720.jpg'},
                {id: 2, name: 'Carver', price: 428000, image: 'https://www.tgyg.com/photos/articles/carver34.jpg'},
                {id: 3, name: 'SX-257', price: 310000, image: 'http://www.carefreeboater.com/wp-content/uploads/2013/07/buy-a-new-boat.jpg'},
                {id: 4, name: 'SuperJet S-1000', price: 300000, image: 'https://cdn.boatinternational.com/bi_prd/bi/library_images/k5K52BvYQ0GB4Rs03TJP_press-buy-yacht-sold-1280x720.jpg'},
                {id: 5, name: 'Predator FishingRoe', price: 300000, image: 'https://images1.boattrader.com/resize/1/53/86/6895386_0_261120181456_0.jpg'},
                {id: 6, name: 'Paneria Sea Fish A1000', price: 300000, image: 'https://sa.kapamilya.com/absnews/abscbnnews/media/ancx/culture/2018/18/bannerpaneria.jpg?ext=.jpg'},
            ]
        }
    }

    render() {
        const offerItems = this.state.offerItems.map(({ id, name, price, image }) => {
            return (
                <div className="list-item"
                     key={ id }>
                    <div className="list-item--image">
                        <img src={ image }
                             alt={ name } />
                    </div>
                    <div className="list-item--price">{ price }</div>
                    <div className="list-item--name">{ name }</div>
                </div>
            )
        });

        return (
            <div className="app">
                <AppHeader />
                <div className="app-container">
                    <SearchFilter />

                    <h1>Яхты в Сочи</h1>
                    <h3>Приглядитесь к этим предложениям</h3>
                    <div className="items-list">
                        { offerItems }
                    </div>
                </div>
            </div>
        )
    }
}